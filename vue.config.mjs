import routes from './src/router/routes.js';

export default {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
            @import "@/scss/_variables.scss";
          `,
      },
    },
  },
  pluginOptions: {
    sitemap: {
      baseURL: 'https://pervasive-frontend.jore.dev',
      routes,
      defaults: {
        changefreq: 'always',
      },
    },
  },
};
