import { ref, readonly, computed, watch, watchEffect } from 'vue';
import { User, NullableRef, UserRole } from '@/types';
import router from '@/router';
// eslint-disable-next-line
// @ts-ignore
import { LOGIN_ROUTE } from '@/router/names';
import useApi from './useApi';

const SESSION_STORAGE_KEY = 'user';

// Load a user from session storage if he exists
const user: NullableRef<User> = ref(
  JSON.parse(sessionStorage.getItem(SESSION_STORAGE_KEY) || 'null') || {
    // TODO: remove automatic login
    id: '',
    name: 'Android',
    role: UserRole.AMBULANCE,
  }
);
const userColor = computed(() =>
  user.value?.role == UserRole.HOSPITAL ? '#007af7' : '#db1c3d'
);
const loading = ref(false);
const readonlyLoading = readonly(loading);

watch(user, () => {
  // Save the user in session storage when a change happens
  sessionStorage.setItem(SESSION_STORAGE_KEY, JSON.stringify(user.value));
});

watchEffect(() => {
  // Set the user color for UI purposes
  document.documentElement.style.setProperty('--user-color', userColor.value);
});

const { api } = useApi();

interface LoginDetails {
  username: string;
  password: string;
}

// Fetch the user from the API and save it
// TODO: make actual API call when API is ready
const login = async (loginDetails: LoginDetails) => {
  // Fake timeout for loading

  loading.value = true;

  try {
    const { data } = await api.post('/login', loginDetails);
    user.value = data;
  } finally {
    loading.value = false;
  }
};

// Log the user out
// TODO: make actual API call when API is ready
const logout = async () => {
  loading.value = true;

  user.value = null;

  loading.value = false;

  await router.push({ name: LOGIN_ROUTE });
};

export default function useUser(): useUserReturn {
  return {
    user,
    login,
    logout,
    loading: readonlyLoading,
  };
}

interface useUserReturn {
  user: typeof user;
  login: typeof login;
  logout: typeof logout;
  loading: typeof readonlyLoading;
}
