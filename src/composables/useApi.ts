import axios from 'axios';

axios.defaults.headers.post['Content-Type'] = 'application/json';

const api = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
});

const useApi = (): useApiReturn => {
  return { api };
};

interface useApiReturn {
  api: typeof api;
}

export default useApi;
