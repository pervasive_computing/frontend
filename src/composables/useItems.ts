import { Item } from '@/types';
import useApi from './useApi';
import { AxiosResponse } from 'axios';

const { api } = useApi();

const updateItem = async (item: Item) => {
  (await api.patch(`/items/${item.id}/update`, item)) as AxiosResponse<Item>;
};

const deleteItem = async (item: Item) => {
  await api.delete(`/items/${item.id}/delete`);
};

export default (): useItemsReturn => {
  return {
    updateItem,
    deleteItem,
  };
};

interface useItemsReturn {
  updateItem: typeof updateItem;
  deleteItem: typeof deleteItem;
}
