import { ref, Ref, watch, watchEffect } from 'vue';
import { Backpack } from '@/types';
import useApi from './useApi';
import { AxiosResponse } from 'axios';
import io from 'socket.io-client';
import * as timeago from 'timeago.js';

const backpacks: Ref<Backpack[]> = ref([]);

const { api } = useApi();

const fetchBackpacks = async () => {
  const { data } = (await api.get(`/backpacks`)) as AxiosResponse<Backpack[]>;

  backpacks.value = data;
};

const backpack: Ref<Backpack | null> = ref(null);
const backpackLastLoadedDate: Ref<Date | null> = ref(null);
const backpackLastLoaded: Ref<string | null> = ref(null);

watch(backpack, () => (backpackLastLoadedDate.value = new Date()));
watchEffect(() => {
  setInterval(() => {
    if (backpackLastLoadedDate.value)
      backpackLastLoaded.value = timeago.format(backpackLastLoadedDate.value);
  }, 1000);
});

const fetchBackpack = async (id: Backpack['id']) => {
  const { data } = (await api.get(
    `/backpacks/${id}`
  )) as AxiosResponse<Backpack>;

  backpack.value = data;

  socket.on('backpacks/' + id, (arg: string) => {
    const newBackpack = JSON.parse(arg) as Backpack;
    if (backpack.value?.id == newBackpack.id) {
      backpack.value = newBackpack;
    } else {
      socket.removeListener('backpacks/' + id);
    }
  });
};

const socket = io(process.env.VUE_APP_WS_URL);

export default (): useBackpacksReturn => {
  return {
    backpacks,
    fetchBackpacks,
    backpack,
    backpackLastLoaded,
    fetchBackpack,
  };
};

interface useBackpacksReturn {
  backpacks: typeof backpacks;
  fetchBackpacks: typeof fetchBackpacks;
  backpack: typeof backpack;
  backpackLastLoaded: typeof backpackLastLoaded;
  fetchBackpack: typeof fetchBackpack;
}
