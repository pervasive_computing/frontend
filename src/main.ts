import { createApp } from 'vue';
import App from './App.vue';
import Oruga from '@oruga-ui/oruga-next';
import './scss/global.scss';
import router from './router';

createApp(App).use(router).use(Oruga).mount('#app');
