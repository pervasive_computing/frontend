import { Ref } from 'vue';

export interface User {
  id: string;
  name: string;
  role: UserRole;
}

export enum UserRole {
  HOSPITAL = 'hospital',
  AMBULANCE = 'ambulance',
}

export interface Backpack {
  id: string;
  name: string;
  expiredItems: number;
  items: Item[];
  item_types: ItemType[];
}

export interface Item {
  id: string;
  name: string | null;
  count: number;
  expiration_date: string | null;
  expired: boolean;
  type_id?: ItemType['id'];
  backpackId: Backpack['id'];
}

export interface EditingItem extends Omit<Item, 'expiration_date'> {
  expiration_date: Date | null;
}

export interface ItemType {
  id: number;
  name: string;
  count: number;
  items: Item[];
}

export type NullableRef<T> = Ref<T | null>;
