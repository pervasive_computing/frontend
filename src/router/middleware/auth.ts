import useUser from '@/composables/useUser';
import { RouteLocationNormalized, NavigationGuardNext } from 'vue-router';
// eslint-disable-next-line
// @ts-ignore
import { LOGIN_ROUTE, OVERVIEW_ROUTE } from '@/router/names';

export default function auth(
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext
): void {
  const { user } = useUser();

  // TODO: temp disable auth
  const needsAuth = to.meta.needsAuth !== false;

  // Redirect if page is forbidden
  if (needsAuth && !user.value) {
    next({ name: LOGIN_ROUTE });
  } else if (!needsAuth && user.value) {
    next({ name: OVERVIEW_ROUTE });
  } else {
    next();
  }
}
