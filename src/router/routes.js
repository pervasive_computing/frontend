const {
  LOGIN_ROUTE,
  OVERVIEW_ROUTE,
  BACKPACK_ROUTE,
  SETTINGS_ROUTE,
  // eslint-disable-next-line @typescript-eslint/no-var-requires
} = require('./names');

module.exports = [
  {
    path: '/',
    redirect: {
      name: LOGIN_ROUTE,
    },
    meta: {
      sitemap: {
        changefreq: 'weekly',
        priority: 0.8,
      },
    },
  },
  {
    path: '/overview',
    name: OVERVIEW_ROUTE,
    component: () => import('../views/Overview.vue'),
    meta: {
      sitemap: {
        priority: 0.8,
      },
    },
  },
  {
    path: '/login',
    name: LOGIN_ROUTE,
    component: () => import('../views/Login.vue'),
    meta: {
      needsAuth: false,
      sitemap: {
        changefreq: 'weekly',
        priority: 0.8,
      },
    },
  },
  {
    path: '/backpack/:id',
    name: BACKPACK_ROUTE,
    component: () => import('../views/BackpackView.vue'),
    meta: {
      sitemap: {
        slugs: ['0', '1'],
        priority: 0.6,
      },
    },
  },
  {
    path: '/settings',
    name: SETTINGS_ROUTE,
    component: () => import('../views/Settings.vue'),

    meta: {
      sitemap: {
        priority: 0.3,
      },
    },
  },
];
