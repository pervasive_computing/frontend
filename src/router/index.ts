import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import auth from './middleware/auth';
// eslint-disable-next-line
// @ts-ignore
import routes from './routes.js';

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: routes as Array<RouteRecordRaw>,
});

router.beforeEach(auth);

export default router;
