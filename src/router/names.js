module.exports = {
  LOGIN_ROUTE: 'Login',
  OVERVIEW_ROUTE: 'Overview',
  BACKPACK_ROUTE: 'Backpack',
  SETTINGS_ROUTE: 'Settings',
};
