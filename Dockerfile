# dependencies: use latest node.js version
FROM node:lts

# copy code to container
WORKDIR /usr/app
COPY ./ /usr/app/

# install dependencies
RUN npm install

# start the application
CMD ["npm", "run", "serve"]