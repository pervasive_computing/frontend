// @ts-ignore

var history = require('connect-history-api-fallback');
var express = require('express');

var app = express();
app.use(history());
app.use(express.static('dist'));

app.listen(8080, () => {
  console.log(`Server started`);
});